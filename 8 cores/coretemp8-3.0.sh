#!/bin/bash
# # # # # # # # # # # # # # # # # # # # # # # # #
#		####################		#
#		#CPU Coretemp V3.0 #		#
#		####################		#
#						#
# Only for systems with 8 cores			#
# programmed by Linus Hertzberg			#
# linus.hertzberg@web.de			#
#						#
# Please don't delete the tree.log file		#
# If you have questions about the program	#
# Please write me an Email.			#
# # # # # # # # # # # # # # # # # # # # # # # # #

read -p "How often should be updated the temperatures in seconds" t

##make a tree
find -D search temp1_input /sys/devices/platform/ > tree.log

#read the Temp file
critcoretemp=$(grep -w 'temp1_crit' ./tree.log)
crit1=$(cat $critcoretemp)
syscpu0core0=$(grep 'temp1_input' ./tree.log)
syscpu0core1=$(grep 'temp2_input' ./tree.log)
syscpu0core2=$(grep 'temp3_input' ./tree.log)
syscpu0core3=$(grep 'temp4_imput' ./tree.log)
syscpu0core4=$(grep 'temp5_input' ./tree.log)
syscpu0core5=$(grep 'temp6_input' ./tree.log)
syscpu0core6=$(grep 'temp7_input' ./tree.log)
syscpu0core7=$(grep 'temp8_input' ./tree.log)

while [ true ] ; do

	#read the core temperature
	coretemp00=$(cat $syscpu0core0)
	coretemp01=$(cat $syscpu0core1)
	coretemp02=$(cat $syscpu0core2)
	coretemp03=$(cat $syscpu0core3)
	coretemp04=$(cat $syscpu0core4)
	coretemp05=$(cat $syscpu0core5)
	coretemp06=$(cat $syscpu0core6)
	coretemp07=$(cat $syscpu0core7)

	#temperature calculate
	cpu0core0=$(echo "scale=0; $coretemp00 /1000" | bc -l)
	cpu0core1=$(echo "scale=0; $coretemp01 /1000" | bc -l)
	cpu0core2=$(echo "scale=0; $coretemp02 /1000" | bc -l)
	cpu0core3=$(echo "scale=0; $coretemp03 /1000" | bc -l)
	cpu0core4=$(echo "scale=0; $coretemp04 /1000" | bc -l)
	cpu0core5=$(echo "scale=0; $coretemp05 /1000" | bc -l)
	cpu0core6=$(echo "scale=0; $coretemp06 /1000" | bc -l)
	cpu0core7=$(echo "scale=0; $coretemp07 /1000" | bc -l)
	criticalcoretemp=$(echo "scale=0; $crit1 /1000" | bc -l)

	#average calculate
	cpu0core=$(echo "scale=0; $cpu0core0+$cpu0core1+$cpu0core2+$cpu0core3+$cpu0core4+$cpu0core5+$cpu0core6+$cpu0core7" | bc -l)
	cpu0=$(echo "scale=0; $cpu0core /8" | bc -l)

	#Core 0
	if [ "$criticalcoretemp" -le "$cpu0core0" ]; then
		kritcpu0core0=$(echo -e " \033[31m$cpu0core0")
	else
		kritcpu0core0=$(echo -e " \033[32m$cpu0core0")
	fi

	#Core 1
	if [ "$criticalcoretemp" -le "$cpu0core1" ]; then
		kritcpu0core1=$(echo -e " \033[31m$cpu0core1")
	else
		kritcpu0core1=$(echo -e " \033[32m$cpu0core1")
	fi

	#Core 2
	if [ "$criticalcoretemp" -le "$cpu0core2" ]; then
		kritcpu0core2=$(echo -e " \033[31m$cpu0core2")
	else
		kritcpu0core2=$(echo -e " \033[32m$cpu0core2")
	fi

	#Core 3
	if [ "$criticalcoretemp" -le "$cpu0core3" ]; then
		kritcpu0core3=$(echo -e " \033[31m$cpu0core3")
	else
		kritcpu0core3=$(echo -e " \033[32m$cpu0core3")
	fi

	#Core 4
	if [ "$criticalcoretemp" -le "$cpu0core4" ]; then
		kritcpu0core4=$(echo -e " \033[31m$cpu0core4")
	else
		kritcpu0core4=$(echo -e " \033[32m$cpu0core4")
	fi

	#Core 5
	if [ "$criticalcoretemp" -le "$cpu0core5" ]; then
		kritcpu0core5=$(echo -e " \033[31m$cpu0core5")
	else
		kritcpu0core5=$(echo -e " \033[32m$cpu0core5")
	fi

	#Core 6
	if [ "$criticalcoretemp" -le "$cpu0core6" ]; then
		kritcpu0core6=$(echo -e " \033[31m$cpu0core6")
	else
		kritcpu0core6=$(echo -e " \033[32m$cpu0core6")
	fi

	#Core 7
	if [ "$criticalcoretemp" -le "$cpu0core7" ]; then
		kritcpu0core7=$(echo -e " \033[31m$cpu0core7")
	else
		kritcpu0core7=$(echo -e " \033[32m$cpu0core7")
	fi

	#average CPU 0
	if [ "$criticalcoretemp" -le "$cpu0" ]; then
		kritcpu0=$(echo -e " \033[31m$cpu0")
	else
		kritcpu0=$(echo -e " \033[32m$cpu0")
	fi

	clear
	echo -e "CPU 1 $kritcpu0°C\033[0m"
	echo -e "\033[1m---\033[0m"
	echo -e "Core 1: $kritcpu0core0°C\033[0m"
	echo -e "Core 2: $kritcpu0core1°C\033[0m"
	echo -e "Core 3: $kritcpu0core2°C\033[0m"
	echo -e "Core 4: $kritcpu0core3°C\033[0m"
	echo -e "Core 5: $kritcpu0core4°C\033[0m"
	echo -e "Core 6: $kritcpu0core5°C\033[0m"
	echo -e "Core 7: $kritcpu0core6°C\033[0m"
	echo -e "Core 8: $kritcpu0core7°C\033[0m"
	echo " "
	sleep $t
done





