#!/bin/bash

# # # # # # # # # # # # # # # # # # # # # # # # # # #
#			####################					#
#			#CPU Coretemp V2.2 #					#
#			####################					#
#													#
# Only for systems with 2 cores						#
# programmed by Linus Hertzberg						#
#													#	
# Please configure the path to the Coretemp			#
# file. It is mostly a "temp*_input" file			#
# # # # # # # # # # # # # # # # # # # # # # # # # # #
read -p "How often should be updated the temperatures in seconds" t
while [ true ] ; do


	##Please configure
	criticalcoretemp=80
	syscpu0core0=/sys/devices/platform/coretemp.0/temp2_input
	syscpu0core1=/sys/devices/platform/coretemp.0/temp3_input


	#read the core temperature
	coretemp00=$(cat $syscpu0core0)
	coretemp01=$(cat $syscpu0core1)


	#temperature calculate
	cpu0core0=$(echo "scale=0; $coretemp00 /1000" | bc -l)
	cpu0core1=$(echo "scale=0; $coretemp01 /1000" | bc -l)
	
	#average calculate
	cpu0core=$(echo "scale=0; $cpu0core0+$cpu0core1" | bc -l)
	cpu0=$(echo "scale=0; $cpu0core /2" | bc -l)
	
	#Core 0
	if [ "$criticalcoretemp" -le "$cpu0core0" ]; then
			kritcpu0core0=$(echo -e " \033[31m$cpu0core0")
		else
			kritcpu0core0=$(echo -e " \033[32m$cpu0core0")
	fi
	
	#Core 1
	if [ "$criticalcoretemp" -le "$cpu0core1" ]; then
			kritcpu0core1=$(echo -e " \033[31m$cpu0core1")
		else
			kritcpu0core1=$(echo -e " \033[32m$cpu0core1")
	fi
	
	#average CPU 0
	if [ "$criticalcoretemp" -le "$cpu0" ]; then
			kritcpu0=$(echo -e " \033[31m$cpu0")
		else
			kritcpu0=$(echo -e " \033[32m$cpu0")
	fi
	
	clear
	echo -e "CPU 1 $kritcpu0°C\033[0m"
	echo -e "\033[1m---\033[0m"
	echo -e "Core 1: $kritcpu0core0°C\033[0m"
	echo -e "Core 2: $kritcpu0core1°C\033[0m"
	echo " "
	sleep $t
done
